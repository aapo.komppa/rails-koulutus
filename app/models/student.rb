class Student < ApplicationRecord
  belongs_to :school
  enum sex: %w[male female other].freeze
  enum favourite_subject: %w[
    Art
    Geography
    History
    Language
    Literacy
    Music
    Science
    Arithmetic
    Reading
    Writing
  ].freeze
end
