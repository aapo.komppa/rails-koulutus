class School < ApplicationRecord
  has_many :students

  # 3.1 Creating an Object
  #   before_validation
  #   after_validation
  #   before_save
  #   around_save
  #   before_create
  #   around_create
  #   after_create
  #   after_save
  #   after_commit / after_rollback
  # 3.2 Updating an Object
  #   before_validation
  #   after_validation
  #   before_save
  #   around_save
  #   before_update
  #   around_update
  #   after_update
  #   after_save
  #   after_commit / after_rollback
  # 3.3 Destroying an Object
  #   before_destroy
  #   around_destroy
  #   after_destroy
  #   after_commit / after_rollback
end
