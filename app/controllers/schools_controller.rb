class SchoolsController < ApplicationController
  before_action do
    @value = 1
  end

  def index
    render json: School.find_each
  end

  def show
    do_some_stuff
    render json: School.find(params[:id])
  end

  def create
    render json: School.create(permitted_params)
  end

  private

  # just to show stuff
  def do_some_stuff
    value = 1
    return_forbidden unless value == @value
    render json: { name: 'wrong' } && return if save_name 'asdasd' == @name
  end

  def save_name(name)
    @name = name
  end

  def return_forbidden
    render json: {
      errors: {
        name: 'wrong name'
      }
    }, status: :forbidden && return
  end

  def permitted_params
    params.require(:school).permit(:name, :address)
  end
end
