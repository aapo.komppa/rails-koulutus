class AddFieldsToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :sex, :integer
    add_column :students, :favourite_subject, :integer
    add_column :students, :school_id, :integer
  end
end
