class AddFieldsToSchool < ActiveRecord::Migration[6.1]
  def change
    add_column :schools, :name, :string
    add_column :schools, :address, :string
  end
end
