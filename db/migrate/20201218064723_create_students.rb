class CreateStudents < ActiveRecord::Migration[6.1]
  def change
    create_table :students do |t|
    	t.string   :name
      t.string   :address
      t.date     :born_on

      t.timestamps
    end
  end
end
